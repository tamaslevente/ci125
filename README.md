# README #

This repo contains the report/results for the collaboration between Braintronix-UTCN.

### What is this repository for? ###
This repo contains 3D data processing tools:

* Tutorial for processing 3D data
+ 3D Object template creartion
- 3D Object recognition 


### How do I get set up? ###
The best experience can be achieved on native hosts. Howver you can use virtual hosst as well:
* Summary of set up *

Check for detials
~~~~~~~~~~~~~~~~~~~~~
http://rocon.utcluj.ro/~levente/?page_id=179 
~~~~~~~~~~~~~~~~~~~~~

* Configuration 

We used the ROS catkin worspace setup for code. 
* Dependencies 

PCL 1.7

* Database configuration
* How to run tests
* Deployment instructions

Connect a streaming 3D camera to the PC.

### 3D Object training ###

In order to make your own template DB, you should perform the following steps

* Create a directory structure with the names of your objects (e.g. box, firee, etc) and a higher level dir conaining these subdirs (e.g. objects)
* Launch the openni2 grabber with: 
~~~~~~~~~~~~~~~~~~~~~
roslaunch openni2_launch openni2.launch
~~~~~~~~~~~~~~~~~~~~~

* Start the processing pipeline with
~~~~~~~~~~~~~~~~~~~~~
roslaunch pcl_tutorial tutorial_online.launch
~~~~~~~~~~~~~~~~~~~~~

* Record the object from different view points with the command: 
~~~~~~~~~~~~~~~~~~~~~
rosbag record -l 1  /plan_out_2 /camera/depth/image_rect_raw /camera/rgb/image_rect_color /camera/depth_registered/points /camera/rgb/camera_info /camera/depth_registered/camera_info
~~~~~~~~~~~~~~~~~~~~~

* Extract the recorded bag files with the *preprocess_data.sh* for each of the objects (i.e. cd in the object dir, and run the script). This will extract pcd/jpg files for the current object
* Move the segments dir content in the object dir (you may delete the rest if you are not using them)
* Run the rosrun pcl_tutorial build_tree_node ./objects to create the list of template features 
~~~~~~~~~~~~~~~~~~~~~
 rosrun pcl_tutorial build_tree_node ./objects
~~~~~~~~~~~~~~~~~~~~~

### 3D Object recognition ###

In order to run the 3D object recognition part based on VFH descriptors run the followings:

* Launch the openni grabber: 
~~~~~~~~~~~~~~~~~~~~~
roslaunch openni2_launch openni2.launch
~~~~~~~~~~~~~~~~~~~~~

* Start the preprocessing pipeline with: 
~~~~~~~~~~~~~~~~~~~~~
roslaunch pcl_tutorial tutorial_online.launch
~~~~~~~~~~~~~~~~~~~~~

* Run the dynamic reconfig node: 
~~~~~~~~~~~~~~~~~~~~~
rosrun rqt_reconfigure rqt_reconfigure
~~~~~~~~~~~~~~~~~~~~~

* Launch the recognition node with in the same dir where the object lists were created in the training phase.
~~~~~~~~~~~~~~~~~~~~~
rosrun pcl_tutorial get_3d_model_node kdtree.idx training_data.h5 training_data.list 
~~~~~~~~~~~~~~~~~~~~~

* Be sure to have every node up and running. 
* Check for the camera driver registration flag in the reconfig list
* Adjust the filter/segmentation params to be close to the ones used in the training phase
* Check the recognition th as well (lower means more strict recognition)


### Who do I talk to? ###
Levente.Tamas@aut.utcluj.ro
