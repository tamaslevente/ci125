#!/bin/bash

shopt -s globstar
#rosrun pcl_ros pointcloud_to_pcd input:=/plan_out_2 _prefix:=./segments/&
#rosrun pcl_ros pointcloud_to_pcd input:=/camera/depth_registered/points _prefix:=./scene/&
cd ./rgb
rosrun image_view image_saver image:=/camera/rgb/image_rect_color _encoding:=rgb8&
cd ../depth
rosrun image_view image_saver image:=/camera/depth/image_rect_raw _encoding:=16UC1&
cd ..
for file in ./*.bag; do 
    if [[ -f "$file" ]]; then
        dirname="${file%/*}/"
        basename="${file:${#dirname}}"
	rosbag play -d 2 "$file"
	sleep 1
	echo processesd "$file"
    fi
done
killall pointcloud_to_pcd
killall image_saver
