#!/bin/bash

for i in $(ls -d */);do  
	basename="${i%%/}"
	echo $basename;
	cd $basename;
	mkdir segments
	mkdir scene
	mkdir rgb
	mkdir depth
	../preprocess_data.sh
	cd ..
done

