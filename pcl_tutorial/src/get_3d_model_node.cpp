#include <ros/ros.h>

// PCL specific includes
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/console/print.h>
#include <pcl/io/pcd_io.h>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <iostream>
#include <flann/flann.h>
#include <flann/io/hdf5.h>
#include <boost/filesystem.hpp>
#include <pcl/features/vfh.h>
#include <pcl/features/normal_3d.h>

#include <sensor_msgs/PointCloud2.h>
// PCL specific includes
#include <pcl/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>

//dynamicrec
#include <dynamic_reconfigure/server.h>
#include <pcl_tutorial/preproc_nodeConfig.h>


typedef std::pair<std::string, std::vector<float> > vfh_model;

//Global variables

ros::Publisher pub;

double thresh;
bool classify = true;

std::string kdtree_idx_file_name = "kdtree.idx";
std::string training_data_h5_file_name = "training_data.h5";
std::string training_data_list_file_name = "training_data.list";


std::vector<vfh_model> models;
flann::Matrix<int> k_indices;
flann::Matrix<float> k_distances;
flann::Matrix<float> data;
int k = 1;


/** \brief Loads an n-D histogram file as a VFH signature
  * \param path the input file name
  * \param vfh the resultant VFH model
  */
bool
loadHist (const boost::filesystem::path &path, vfh_model &vfh)
{
  int vfh_idx;
  // Load the file as a PCD
  try
  {
    // pcl::PointCloud2 cloud;
    // int version;
    // Eigen::Vector4f origin;
    // Eigen::Quaternionf orientation;
    pcl::PCDReader r;
    int type; int idx;
    pcl::PCLPointCloud2::Ptr cloud (new pcl::PCLPointCloud2 ()); 
    r.readHeader (path.string (), *cloud);

   vfh_idx = pcl::getFieldIndex (*cloud, "vfh");
    if (vfh_idx == -1)
      return (false);
    if ((int)cloud->width * cloud->height != 1)
      return (false);
  }
  catch (pcl::InvalidConversionException e)
  {
    return (false);
  }

  // Treat the VFH signature as a single Point Cloud
  pcl::PointCloud <pcl::VFHSignature308> point;
  pcl::io::loadPCDFile (path.string (), point);
  vfh.second.resize (308);

  // std::vector <sensor_msgs::PointField> fields;
  // getFieldIndex (point, "vfh", fields);

  for (size_t i = 0; i < 308; ++i)
  {
    vfh.second[i] = point.points[0].histogram[i];
  }
  vfh.first = path.string ();
  return (true);
}


/** \brief Search for the closest k neighbors
  * \param index the tree
  * \param model the query model
  * \param k the number of neighbors to search for
  * \param indices the resultant neighbor indices
  * \param distances the resultant neighbor distances
  */
inline void
nearestKSearch (flann::Index<flann::ChiSquareDistance<float> > &index, const vfh_model &model,
                int k, flann::Matrix<int> &indices, flann::Matrix<float> &distances)
{
  // Query point
  flann::Matrix<float> p = flann::Matrix<float>(new float[model.second.size ()], 1, model.second.size ());
  memcpy (&p.ptr ()[0], &model.second[0], p.cols * p.rows * sizeof (float));

  indices = flann::Matrix<int>(new int[k], 1, k);
  distances = flann::Matrix<float>(new float[k], 1, k);
  index.knnSearch (p, indices, distances, k, flann::SearchParams (512));
  delete[] p.ptr ();
}

/** \brief Load the list of file model names from an ASCII file
  * \param models the resultant list of model name
  * \param filename the input file name
  */
bool
loadFileList (std::vector<vfh_model> &models, const std::string &filename)
{
  ifstream fs;
  fs.open (filename.c_str ());
  if (!fs.is_open () || fs.fail ())
    return (false);

  std::string line;
  while (!fs.eof ())
  {
    getline (fs, line);
    if (line.empty ())
      continue;
    vfh_model m;
    m.first = line;
    models.push_back (m);
  }
  fs.close ();
  return (true);
}


/** \brief Loads an n-D histogram file as a VFH signature
 * \param path the input file name
 * \param vfh the resultant VFH model
 */
void
computeVFH (pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, pcl::PointCloud<pcl::VFHSignature308>::Ptr &vfhs)
{
  pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal> ());

  // Create the normal estimation class, and pass the input dataset to it
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
  ne.setInputCloud (cloud);

  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  ne.setSearchMethod (tree);

  // Output datasets
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

  // Use all neighbors in a sphere of radius 3cm
  ne.setRadiusSearch (0.03);

  // Compute the features
  ne.compute (*normals);

  // Create the VFH estimation class, and pass the input dataset+normals to it
  pcl::VFHEstimation<pcl::PointXYZ, pcl::Normal, pcl::VFHSignature308> vfh;
  vfh.setInputCloud (cloud);
  vfh.setInputNormals (normals);
  // alternatively, if cloud is of type PointNormal, do vfh.setInputNormals (cloud);

  // Create an empty kdtree representation, and pass it to the FPFH estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  vfh.setSearchMethod (tree);

  // Compute the features
  vfh.compute (*vfhs);

}

void 
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  // Load the test histogram
  vfh_model histogram;

  // Output datasets
  pcl::PointCloud<pcl::VFHSignature308>::Ptr vfhs (new pcl::PointCloud<pcl::VFHSignature308> ());
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromROSMsg (*input, *cloud_filtered);

  if (cloud_filtered->points.size()<50)
  {
    pcl::console::print_info("No object to classify\n");
    return;
  }


  computeVFH(cloud_filtered, vfhs);
  histogram.second.resize (308);

  //for (size_t i = 0; i < sizeof(vfhs->points[0].histogram)/sizeof(vfhs->points[0].histogram[0]); ++i)
  for (size_t i = 0; i < 308; ++i)
    histogram.second[i] = vfhs->points[0].histogram[i];

  histogram.first = "Detected";

  flann::Index<flann::ChiSquareDistance<float> > index (data, flann::SavedIndexParams ("kdtree.idx"));
  index.buildIndex ();
  nearestKSearch (index, histogram, k, k_indices, k_distances);

  int i = 0;

  if(k_distances[0][i] < thresh)
    pcl::console::print_info (" %s (%d) with a distance of: %f\n",
          models.at (k_indices[0][i]).first.c_str (), k_indices[0][i], k_distances[0][i]);

}


void callback_dr(pcl_tutorial::preproc_nodeConfig &config, uint32_t level) {
  ROS_DEBUG("Reconfigure Request: %f %s",
            config.thresh,
            config.classify?"True":"False");

  thresh = config.thresh;
  classify = config.classify;
}


void init_classification(int argc, char** argv)
{
  if (argc < 2)
   {
     pcl::console::print_error
       ("Need at least three parameters! Syntax is: %s <query_vfh_model.pcd> [options] {kdtree.idx} {training_data.h5} {training_data.list}\n", argv[0]);
     pcl::console::print_info ("    where [options] are:  -k      = number of nearest neighbors to search for in the tree (default: ");
     pcl::console::print_value ("%d", k); pcl::console::print_info (")\n");
     pcl::console::print_info ("                          -thresh = maximum distance threshold for a model to be considered VALID (default: ");
     pcl::console::print_value ("%f", thresh); pcl::console::print_info (")\n\n");
     exit (-1);
   }

   std::string extension (".pcd");
   transform (extension.begin (), extension.end (), extension.begin (), (int(*)(int))tolower);

   pcl::console::parse_argument (argc, argv, "-thresh", thresh);
   // Search for the k closest matches
   pcl::console::parse_argument (argc, argv, "-k", k);
   pcl::console::print_highlight ("Using "); pcl::console::print_value ("%d", k); pcl::console::print_info (" nearest neighbors.\n");

   std::string kdtree_idx_file_name = "kdtree.idx";
   std::string training_data_h5_file_name = "training_data.h5";
   std::string training_data_list_file_name = "training_data.list";


  // Check if the data has already been saved to disk
  if (!boost::filesystem::exists ("training_data.h5") || !boost::filesystem::exists ("training_data.list"))
  {
    pcl::console::print_error ("Could not find training data models files %s and %s!\n",
        training_data_h5_file_name.c_str (), training_data_list_file_name.c_str ());
    exit  (-1);
  }
  else
  {
    loadFileList (models, training_data_list_file_name);
    flann::load_from_file (data, training_data_h5_file_name, "training_data");
    pcl::console::print_highlight ("Training data found. Loaded %d VFH models from %s/%s.\n",
        (int)data.rows, training_data_h5_file_name.c_str (), training_data_list_file_name.c_str ());
  }

  // search for correspondances
  // Check if the tree index has already been saved to disk
  if (!boost::filesystem::exists (kdtree_idx_file_name))
  {
    pcl::console::print_error ("Could not find kd-tree index in file %s!", kdtree_idx_file_name.c_str ());
    exit (-1);
  }
}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "classify_objects");
  ros::NodeHandle nh;

  // dyna rec
  dynamic_reconfigure::Server<pcl_tutorial::preproc_nodeConfig> server;
  dynamic_reconfigure::Server<pcl_tutorial::preproc_nodeConfig>::CallbackType f;

  f = boost::bind(&callback_dr, _1, _2);
  server.setCallback(f);


  // Create a ROS subscriber for the input point cloud

  ros::Subscriber sub = nh.subscribe ("/plan_out_2", 1, cloud_cb);


  // Classification
  init_classification(argc, argv);

  // Spin
  ROS_INFO("Classifier up and running ... \n");
  ros::spin ();
}
