#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/console/print.h>
#include <pcl/io/pcd_io.h>
#include <iostream>
#include <fstream>
#include <flann/flann.h>
#include <flann/io/hdf5.h>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/split.hpp>
#include <string>
#include <algorithm>
#include <vector>

typedef std::pair<std::string, std::vector<float> > pc_model;
typedef std::pair<std::string, std::string> string_pair_type;
typedef std::vector<string_pair_type> string_pair_vector;

typedef std::pair<std::string, std::vector<float> > vfh_model;

/** \brief Loads an n-D histogram file as a VFH signature
  * \param path the input file name
  * \param vfh the resultant VFH model
  */
bool
loadHist (const boost::filesystem::path &path, vfh_model &vfh, int datacols)
{
  int vfh_idx;
  // Load the file as a PCD
  try
  {
    pcl::PCLPointCloud2 cloud;
    int version;
    Eigen::Vector4f origin;
    Eigen::Quaternionf orientation;
    pcl::PCDReader r;
    int type;
    unsigned int idx;
    r.readHeader (path.string (), cloud, origin, orientation, version, type, idx);

    vfh_idx = pcl::getFieldIndex (cloud, "vfh");
    if (vfh_idx == -1)
      return (false);
    if ((int)cloud.width * cloud.height != 1)
      return (false);
  }
  catch (pcl::InvalidConversionException e)
  {
    return (false);
  }

  // Treat the VFH signature as a single Point Cloud
  pcl::PointCloud <pcl::VFHSignature308> point;
  pcl::io::loadPCDFile (path.string (), point);
  vfh.second.resize (308);

  for (size_t i = 0; i < 308; ++i)
  {
    vfh.second[i] = point.points[0].histogram[i];
  }
  vfh.first = path.string ();
  return (true);
}

/** \brief Search for the closest k neighbors
 * \param index the tree
 * \param model the query model
 * \param k the number of neighbors to search for
 * \param indices the resultant neighbor indices
 * \param distances the resultant neighbor distances
 */
inline void nearestKSearch(flann::Index<flann::ChiSquareDistance<float> > &index, const pc_model &model, int k,
                           flann::Matrix<int> &indices, flann::Matrix<float> &distances)
{
  // Query point
  flann::Matrix<float> p = flann::Matrix<float>(new float[model.second.size()], 1, model.second.size());
  memcpy(&p.ptr()[0], &model.second[0], p.cols * p.rows * sizeof(float));

  indices = flann::Matrix<int>(new int[k], 1, k);
  distances = flann::Matrix<float>(new float[k], 1, k);
  index.knnSearch(p, indices, distances, k, flann::SearchParams(512));
  delete[] p.ptr();
}

/** \brief Load the list of file model names from an ASCII file
 * \param models the resultant list of model name
 * \param filename the input file name
 */
bool loadFileList(std::vector<pc_model> &models, const std::string &filename)
{
  ifstream fs;
  fs.open(filename.c_str());
  if (!fs.is_open() || fs.fail())
    return (false);

  std::string line;
  while (!fs.eof())
  {
    getline(fs, line);
    if (line.empty())
      continue;
    pc_model m;
    m.first = line;
    models.push_back(m);
  }
  fs.close();
  return (true);
}

std::string getObjectName(std::string path)
{

  typedef std::vector<boost::iterator_range<std::string::iterator> > find_vector_type;

  find_vector_type FindVec; // #1: Search for separators
  boost::ifind_all(FindVec, path, "/"); // FindVec == { [abc],[ABC],[aBc] }

  typedef std::vector<std::string> split_vector_type;

  split_vector_type splitVec;
  boost::split(splitVec, path, boost::algorithm::is_any_of("/"));

  if (splitVec.size() > 1)
    return splitVec.at(splitVec.size() - 2);

}

string_pair_vector makeEvaluation(std::vector<pc_model> *models, flann::Index<flann::ChiSquareDistance<float> > *index,
                                  int datacols)
{
  string_pair_vector svp;
  string_pair_type spt;

  for (int i = 0; i < models->size(); i++)
  {
    std::string path = models->at(i).first;
    pc_model histogram;
    int k = 2;
    flann::Matrix<int> k_indices;
    flann::Matrix<float> k_distances;

    if (!loadHist(path.c_str(), histogram, datacols))
    {
      pcl::console::print_error("Cannot load test file %s\n", path.c_str());
      exit(-1);
    }

    nearestKSearch(*index, histogram, k, k_indices, k_distances);
    //printf("Object type -%s- classified as %s\n", getObjectName(path).c_str(), models->at (k_indices[0][1]).first.c_str ());

    spt.first = getObjectName(path);
    spt.second = getObjectName(models->at(k_indices[0][1]).first);

    svp.push_back(spt);
  }

  return svp;
}

void saveEvaluation(string_pair_vector svp, char * filename)
{
  ofstream myfile(filename);
  typedef std::pair<float, float> recall_precision;
  float Tp = 0, Fp = 0, Onr = 1, TotalOnr = svp.size();
  float recall, precision;
  std::string objectClass = svp.at(0).first;// what we compare against the rest

  if (!myfile.is_open())
  {
    cout << "Unable to open classification output file";
    exit(-1);
  }

  for (int i = 0; i < svp.size(); i++)
  {
    if ((objectClass.compare(svp.at(i).first.c_str()) != 0) || (i == svp.size()-1))
    {
      for (int j = 0; j < svp.size(); j++)
      {
        if ( (objectClass.compare(svp.at(j).second) == 0) && (objectClass.compare(svp.at(j).first) == 0) )
          Tp++;

        if ( (objectClass.compare(svp.at(j).second) == 0) && (objectClass.compare(svp.at(j).first) != 0) )
            Fp++;

        if (objectClass.compare(svp.at(j).first) == 0)
            Onr++;
      }

      if (Onr > 0)
        recall = Tp / Onr;
      else
        recall = 0;

      if (Tp+Fp > 0)
        precision = Tp / (Tp + Fp);
      else
        precision = 0;

      myfile << std::fixed << std::setprecision(3) << precision << " " << recall << " " << Fp / (svp.size() - Onr) << " " << Tp / Onr << "\n";

      objectClass = svp.at(i).first;
      Tp = 0;
      Fp = 0;
      Onr = 0;
    }
    printf("Object type -%s- classified as %s\n", svp.at(i).first.c_str(), svp.at(i).second.c_str());
  }

  myfile.close();
}


int main(int argc, char** argv)
{
  int k = 2;

  double thresh = DBL_MAX; // No threshold, disabled by default

  if (argc < 2)
  {
    pcl::console::print_error(
        "Need at least three parameters! Syntax is: %s <query_rsd_model.pcd> [options] {kdtree.idx} {training_data.h5} {training_data.list}\n",
        argv[0]);
    pcl::console::print_info(
        "    where [options] are:  -k      = number of nearest neighbors to search for in the tree (default: ");
    pcl::console::print_value("%d", k);
    pcl::console::print_info(")\n");
    pcl::console::print_info(
        "                          -thresh = maximum distance threshold for a model to be considered VALID (default: ");
    pcl::console::print_value("%f", thresh);
    pcl::console::print_info(")\n\n");
    return (-1);
  }

  std::string extension(".pcd");
  transform(extension.begin(), extension.end(), extension.begin(), (int (*)(int))tolower);

  pcl::console::parse_argument(argc, argv, "-thresh", thresh);
  // Search for the k closest matches
  pcl::console::parse_argument(argc, argv, "-k", k);
  pcl::console::print_highlight("Using ");
  pcl::console::print_value("%d", k);
  pcl::console::print_info(" nearest neighbors.\n");

  std::string kdtree_idx_file_name = "kdtree.idx";
  std::string training_data_h5_file_name = "training_data.h5";
  std::string training_data_list_file_name = "training_data.list";

  std::vector<pc_model> models;
  flann::Matrix<int> k_indices;
  flann::Matrix<float> k_distances;
  flann::Matrix<float> data;
  // Check if the data has already been saved to disk
  if (!boost::filesystem::exists("training_data.h5") || !boost::filesystem::exists("training_data.list"))
  {
    pcl::console::print_error("Could not find training data models files %s and %s!\n",
                              training_data_h5_file_name.c_str(), training_data_list_file_name.c_str());
    return (-1);
  }
  else
  {
    loadFileList(models, training_data_list_file_name);
    flann::load_from_file(data, training_data_h5_file_name, "training_data");
    pcl::console::print_highlight("Training data found. Loaded %d PC models from %s/%s.\n", (int)data.rows,
                                  training_data_h5_file_name.c_str(), training_data_list_file_name.c_str());
  }

  // Load the test histogram
  std::vector<int> pcd_indices = pcl::console::parse_file_extension_argument(argc, argv, ".pcd");
  pc_model histogram;

  // Check if the tree index has already been saved to disk
  if (!boost::filesystem::exists(kdtree_idx_file_name))
  {
    pcl::console::print_error("Could not find kd-tree index in file %s!", kdtree_idx_file_name.c_str());
    return (-1);
  }
  else
  {
    flann::Index<flann::ChiSquareDistance<float> > index(data, flann::SavedIndexParams("kdtree.idx"));
    index.buildIndex();
    string_pair_vector svp = makeEvaluation(&models, &index, data.cols);
    if (svp.size() > 0)
      saveEvaluation(svp, argv[2]);
    else
    {
      pcl::console::print_error("Could not find any object classification output!");
      exit(-1);
    }
  }

  return (0);
}
